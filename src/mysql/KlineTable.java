package mysql;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;

public class KlineTable {

	String tableName = "kline";
	DatabaseConnector con;
	Statement statement;
	
	/**
	 * Create a new instance
	 * 
	 * @throws SQLException
	 */
	public KlineTable () throws SQLException {
		con = new DatabaseConnector();
		statement = con.getStatement();
	}
	
	/**
	 * Create the table
	 * 
	 * @throws SQLException
	 * @return void
	 */
	public void create() throws SQLException {
		String sql = "CREATE TABLE `kline` (\n" + 
				"  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,\n" + 
				"  `ticker` varchar(255),\n" + 
				"  `per` varchar(255),\n" + 
				"  `dt-yyyy-mm-dd` varchar(255),\n" + 
				"  `time` int(11),\n" + 
				"  `open` int(11) DEFAULT NULL,\n" + 
				"  `high` int(11) DEFAULT NULL,\n" + 
				"  `low` int(11) DEFAULT NULL,\n" + 
				"  `close` int(11) DEFAULT NULL,\n" + 
				"  `vol` int(11) DEFAULT NULL,\n" + 
				"  `open-int` int(11) DEFAULT NULL,\n" + 
				"  PRIMARY KEY (`id`),\n" +
				"  UNIQUE KEY `unique_index` (`ticker`, `per`, `dt-yyyy-mm-dd`, `time`)\n" + 
				") ENGINE=InnoDB DEFAULT CHARSET=utf8;";

		statement.executeUpdate(sql);
	}
	
	/**
	 * Insert or update data
	 * 
	 * @param data
	 * @throws SQLException
	 * @return void
	 */
	public void insertOnUpdate(Map<String, String> data) throws SQLException {
		String sql = "INSERT INTO " + tableName + "%s ON DUPLICATE KEY UPDATE %s;";
		statement.executeUpdate(String.format(
				sql,
				prepareInsertValues(data),
				prapareDuplicateKeyUpdate(data)
		));
	}
	
	/**
	 * Prepare the insert SQL
	 * 
	 * @param data
	 * @return String
	 */
	protected String prepareInsertValues(Map<String, String> data) {
		return String.format(
			"(`ticker`, `per`, `dt-yyyy-mm-dd`, `time`, `open`, `high`, `low`, `close`, `vol`, `open-int`) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
			data.get("ticker"),
			data.get("per"),
			data.get("dt-yyyy-mm-dd"),
			data.get("time"),
			data.get("open"),
			data.get("high"),
			data.get("low"),
			data.get("close"),
			data.get("vol"),
			data.get("open-int")
		);
	}
	
	/**
	 * Prepare duplicate key update SQL
	 * 
	 * @param data
	 * @return String
	 */
	protected String prapareDuplicateKeyUpdate(Map<String, String> data) {
		return String.format(
				"`ticker` = %s, `per` = %s, `dt-yyyy-mm-dd` = %s, `time` = %s;",
				data.get("ticker"),
				data.get("per"),
				data.get("dt-yyyy-mm-dd"),
				data.get("time")
		);
	}
	
	/**
	 * Check the table exists or not
	 * 
	 * @throws SQLException
	 * @return void
	 */
	public Boolean exists() throws SQLException {
		return con.exists(tableName);
	}
}

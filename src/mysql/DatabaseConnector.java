package mysql;

import java.sql.*;

public class DatabaseConnector { 

	// TODO: Connect with SSL
	String database = "parser";
	String conUrl = "jdbc:mysql://localhost:3306/" + database + "?" + "user=root";
	String driver = "com.mysql.cj.jdbc.Driver";

	/**
	 * Create a new instance
	 * 
	 * @return void
	 */
	public DatabaseConnector()
	{
		try {
			Class.forName(driver).newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Get the statement
	 * 
	 * @throws SQLException
	 * @return void
	 */
	public Statement getStatement() throws SQLException
	{
		Connection con = DriverManager.getConnection(conUrl);
		Statement statement = con.createStatement();

		return statement;
	}
	
	/**
	 * Check the specific table exists or not
	 * 
	 * @param tableName
	 * @throws SQLException
	 * @return Boolean
	 */
	public Boolean exists(String tableName) throws SQLException {
		Connection con = DriverManager.getConnection(conUrl);
		DatabaseMetaData dbm = con.getMetaData();

        return dbm.getTables(null, database, tableName, null).next();
	}
}

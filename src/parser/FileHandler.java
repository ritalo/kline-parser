package parser;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class FileHandler {
	
	/**
	 * Root folder
	 */
	String root = "./src/data/";
	
	/**
	 * Get the file name which would be parsing
	 * 
	 * @return String
	 */
	public ArrayList<String> getFileList() throws IOException {
		ArrayList<String> files = new ArrayList<String>();
		File f = new File(root);
		if (f.isDirectory()) {
			String[] fileList = f.list();
            for(int i = 0; i < fileList.length; i++) {
            	if (fileList[i].contains("TXT") || fileList[i].contains("txt")) {
            		files.add(root + fileList[i]);
            	}
            }
		}

		return files;
	}
}

package parser;

import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import mysql.KlineTable;

public class klineParser {

	public static void main(String[] args) {
		try {
			store();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Read and parse file into database
	 * 
	 * @throws SQLException
	 * @throws IOException
	 * @return void
	 */
	protected static void store () throws SQLException, IOException {
		KlineTable klineTable = new KlineTable();
		if (!klineTable.exists()) {
			klineTable.create();
		}

		FileHandler handler = new FileHandler();
		ArrayList <String> fileList = handler.getFileList();
		for (int i = 0; i < fileList.size(); i++) {
			FileReader fr = new FileReader(fileList.get(i));
			BufferedReader br = new BufferedReader(fr);
			br.readLine();
			while (br.ready()) {
				klineTable.insertOnUpdate(parseLine(br.readLine()));
			}
			fr.close();
		}
	}
	
	/**
	 * Parsing line
	 * 
	 * @param line
	 * @return Map<String, String>
	 */
	protected static Map<String, String> parseLine(String line) {
		String [] splitted = line.split(",");
		Map<String, String> map = new HashMap<String, String>();

		map.put("ticker", "\"" + splitted[0].toString() + "\"");
		map.put("per", "\"" + splitted[1].toString() + "\"");
		map.put("dt-yyyy-mm-dd", "\"" + splitted[2].toString() + "\"");
		map.put("time", splitted[3]);
		map.put("open", splitted[4]);
		map.put("high", splitted[5]);
		map.put("low", splitted[6]);
		map.put("close", splitted[7]);
		map.put("vol", splitted[8]);
		map.put("open-int", splitted[9]);
		
		return map;
	}
}

# Java project - Kline Parser

## Prerequisites
```
→ java -version
java version "1.8.0_181"
Java(TM) SE Runtime Environment (build 1.8.0_181-b13)
Java HotSpot(TM) 64-Bit Server VM (build 25.181-b13, mixed mode)
```
JDBC download:
[down link](https://dev.mysql.com/downloads/connector/j/5.1.html)

# How to use local docker stack

## Local stack service port

| Service | Port |
| ------- | ---- |
| MySQL   | 3306 |

## How to start local stack
```shell
docker-compose up -d --build
```

You can check service by the following command:

```shell
docker-compose ps
```

## How to use MySQL
```shell
docker-compose exec mysql mysql -u dev_user -p

Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 4
Server version: 5.7.24 MySQL Community Server (GPL)

Copyright (c) 2000, 2018, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> use parser;
Database changed
```

## How to stop service port
```shell
$ docker-compose down
```

